# PROČITAJ ME PAŽLJIVO#

### Što se nalazi u ovom repozitoriju? ###

U ovom repozitoriju se nalaze svi primjeri programa korišteni u kolegiju Objektno orijentirano programiranje iz 2015. godine.
Repozitorij koristi **Git** sustav za verzioniranje.
Podaci se spremaju na BItbucket server.

### Zbog čega koristiti sustav za verzioniranje koda? ###

Sustavi za verzioniranje koda omogućuju praćenje promjena u kodu tijekom vremena. Kada se koriste, programer se može u bilo kojem trenutku po potrebi vratiti na bilo koji trenutak u povijesti pa pogledati 
kako je nešto tada bilo realizirano. Ako se naknadno uoči da se krenulo pogrešnim putem, jednostavno se zanemari sve naknadno rađeno i ponovno krene od posljednje ispravne točke.

### Zbog čega koristiti ovaj sustav za primjere na programerskom kolegiju? ###

Primjeri se često mijenjaju i dorađuju. Pri tome su greške neminovne. Kada se neki primjer ispravi ili doradi, uvijek je problem distribucije ažurne verzije primjera.
Korištenje sustava za verzioniranje, ažurna verzija primjera se u svakom nalazi u repozitoriju. Koga zanima, može u bilo kojem trenutku pogledati bilo koju prošlu verziju primjera.

### Kako koristiti primjere iz ovog repozitorija? ###

Više je načina, ovisno o tome kakvu mogućnost imate te želite li samo pogledati posljednje primjere ili i nešto više. 

#### Najjednostavniji način - online pristup kodu ####

Najjednostavniji način je da na ovoj stranici u web pregledniku čitate sadržaj određenog primjera te ga kopirate i zalijepite u vaše razvojno okruženja. To nije najpraktičnije rješenje i koristite ga u krajnjoj nuždi!

#### Bolji način - skidanje arhive s posljednjom verzijom ####

Malo bolji način je da s ove stranice putem linka Downloads na vaše računalo skinete kompletnu arhivu koja sadrži posljednju verziju programa.

#### Najbolji način - pristup kroz Git sustav za verzioniranje ####

Najbolji način za korištenje ovih primjera je da ih skinete u lokalni git repozitorij te im pristupate lokalno kroz git sustav. Na takav način imate pristup ne samo posljednjoj već i svim prethodnim verzijama, a usput vježbate i korištenje Gita.
U tom slučaju koraci su sljedeći:

1. Instalirajte Git i podesite korisničke postavke
2. Na disku vašeg računala napravite repozitorij u koji ćete smjestiti kod

    ```mkdir *direktorij*
	cd *direktorij*
	git init```
   
3. Klonirajte ovaj repozitorij u vaš lokalni

    `git clone https://kzubrinic@bitbucket.org/kzubrinic/oop.git`   
   
4. Počnite raditi sa sadržajem u direktoriju `*direktorij*/oop`


Preduvjet za to je da na računalu imate instaliran **Git** klijent.

[Upute za instalaciju git klijenta](https://www.atlassian.com/git/tutorials/install-git)

[Osnove korištenja Gita](https://www.atlassian.com/git/tutorials/setting-up-a-repository) 

[Tomo Krajina: Uvod u Git (e-knjiga)](https://tkrajina.github.io/uvod-u-git/git.pdf)
